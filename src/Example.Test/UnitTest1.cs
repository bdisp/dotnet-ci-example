using System;
using Xunit;
using Example.Controllers;

namespace Example.Test
{
    public class UnitTest1
    {
        [Fact]
        public void ReturnNotNullValue()
        {
            var controller = new ValuesController();
            var response = controller.Get();
            Assert.NotNull(response);
        }

        [Fact]
        public void ReturnDefaultValue()
        {
            var controller = new ValuesController();
            var response = controller.Get();
            Assert.Equal(new string[] { "value1", "value2" }, response.Value);
        }

        [Fact]
        public void ReturnParamValue()
        {
            var controller = new ValuesController();
            int param = 5;
            var response = controller.Get(param);
            Assert.Equal($"value: {param}", response.Value);
        }
    }
}